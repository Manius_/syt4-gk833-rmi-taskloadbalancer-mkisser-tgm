package service;

import util.PolicyUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RegistryService {
    public static void main(String[] args) {
        System.setProperty("java.security.policy", PolicyUtil.getUtilPath("server.policy"));
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try (BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in)))
        {
            Registry registry = LocateRegistry.createRegistry(1099);
            System.out.println("Created Registry...");
            while (!sysin.readLine().equals("exit"));
            System.out.println("Exiting Registry Service...");
        } catch (Exception e) {
            System.err.println("Registry Service exception:");
            e.printStackTrace();
        }
    }
}
