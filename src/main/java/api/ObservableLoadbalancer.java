package api;

import java.rmi.RemoteException;

public interface ObservableLoadbalancer extends Compute{
    void registerServer(Compute c) throws RemoteException;
    void unregisterServer(Compute c) throws RemoteException;
}
