package util;

public class PolicyUtil {
    public static String getUtilPath(String policyFileName){
        String policyPath = System.getProperty("user.dir");
        policyPath = policyPath.replace("\\", "/");
        policyPath += "/src/main/resources/" + policyFileName;
        policyPath = "file:" + policyPath;
        return policyPath;
    }
}