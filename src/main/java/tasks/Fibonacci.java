package tasks;

import api.Task;

import java.io.Serializable;
import java.math.BigDecimal;

public class Fibonacci  implements Task<BigDecimal>, Serializable {
    private static final long serialVersionUID = 1337L;

    /**
     * Position to be calculated in the fibonacci sequence
     */
    private final int sequencePosition;

    /**
     * Initialization of Fibonacci Task
     * @param sequencePosition
     */
    public Fibonacci(int sequencePosition){this.sequencePosition = sequencePosition;}

    /**
     * Task to be executed on Remote
     * @return
     */
    @Override
    public BigDecimal execute() {
        return computeFibonacci(sequencePosition);
    }

    /**
     * returns the Nth number in the Fibonacci sequence
     * <a href="https://en.wikibooks.org/w/index.php?title=Algorithm_Implementation/Mathematics/Fibonacci_Number_Program&oldid=3460527#Simpler_Iterative_Version">Source</a>
     */
    public BigDecimal computeFibonacci(int N) {
        BigDecimal lo = new BigDecimal(0);
        BigDecimal hi = new BigDecimal(1);
        for (int i = 0; i < N; i++) {
            hi = lo.add(hi) ;
            lo = hi.subtract(lo) ;
        }
        return lo;
    }
}
