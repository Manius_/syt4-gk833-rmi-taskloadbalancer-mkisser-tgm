package client;

import api.Compute;
import tasks.Fibonacci;
import util.PolicyUtil;

import java.math.BigDecimal;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ComputeFibonacci {
    public static void main(String args[]) {
        System.setProperty("java.security.policy", PolicyUtil.getUtilPath("client.policy"));
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try {
            String name = Boolean.parseBoolean(args[2]) ? "LoadBalancer" : "Compute";
            Registry registry = LocateRegistry.getRegistry(args[0]);
            Compute comp = (Compute) registry.lookup(name);
            Fibonacci task = new Fibonacci(Integer.parseInt(args[1]));
            BigDecimal fib = comp.executeTask(task);
            System.out.println(fib);
        } catch (Exception e) {
            System.err.println("ComputeFibonacci exception:");
            e.printStackTrace();
        }
    }
}
