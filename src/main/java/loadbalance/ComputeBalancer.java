package loadbalance;

import api.ObservableLoadbalancer;
import util.PolicyUtil;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class ComputeBalancer {
    public static void main(String[] args) {
        System.setProperty("java.security.policy", PolicyUtil.getUtilPath("server.policy"));
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }
        try (BufferedReader sysin = new BufferedReader(new InputStreamReader(System.in));
        ) {
            System.setProperty("java.rmi.server.hostname", args[0]);
            String name = "LoadBalancer";
            ComputeLoadBalancer balancer = new ComputeLoadBalancer();
            ObservableLoadbalancer stub =
                    (ObservableLoadbalancer) UnicastRemoteObject.exportObject(balancer, 0);
            Registry registry = LocateRegistry.getRegistry();
            registry.rebind(name, stub);

            System.out.println("ComputeBalancer bound");
            while (!sysin.readLine().equals("exit"));
            UnicastRemoteObject.unexportObject(balancer, true);
        } catch (Exception e) {
            System.err.println("ComputeBalancer exception:");
            e.printStackTrace();
        }
    }
}
