package loadbalance;

import api.Compute;
import api.ObservableLoadbalancer;
import api.Task;

import java.rmi.RemoteException;
import java.util.ArrayList;


public class ComputeLoadBalancer implements ObservableLoadbalancer {
    private ArrayList<Compute> servers = new ArrayList<>();
    private int index = 0;

    public ComputeLoadBalancer(){super();}


    @Override
    public void registerServer(Compute c){
        System.out.println("Server has registered.");
        this.servers.add(c);
    }

    @Override
    public void unregisterServer(Compute c){
        System.out.println("Server has unregistered.");
        this.servers.remove(c);
    }

    @Override
    public <T> T executeTask(Task<T> t) throws RemoteException{
        index = index % servers.size();
        System.out.println("Executing Task, using server: " + index);
        T ret = servers.get(index).executeTask(t);
        index++;
        return ret;
    }
}
