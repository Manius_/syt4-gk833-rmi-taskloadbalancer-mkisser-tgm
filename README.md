# Distributed Computing "*RMI Task Loadbalancer*" 

## Aufgabenstellung
Die detaillierte [Aufgabenstellung](TASK.md) beschreibt die notwendigen Schritte zur Realisierung.

## Implementierung

Ausführen der Applikation:

RegistryService starten.

LoadBalancer starten.

ComputeEngine mit loadbalancer Parameter starten.

computePi

### RegistryService

```
gradle registryService
```



### LoadBalancer

```
gradle loadBalancer --args="localhost"
```



### Engine

```
gradle engine --args="localhost"
```

### computePi

`{registryServiceIp} {computationLength} {loadBalancing}`

```
gradle computePi --args="localhost 123 true"
```



## Quellen
